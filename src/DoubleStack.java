import java.util.LinkedList;

public class DoubleStack {
   private LinkedList<Double> stack;


   public static void main(String[] argum) {

   }

   DoubleStack() {
      stack = new LinkedList<Double>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      if (stEmpty()) {
         throw new IndexOutOfBoundsException("Magasin on tühi");
      }
      DoubleStack tmp = new DoubleStack();
      tmp.stack = (LinkedList) this.stack.clone();
      return tmp;
   }

   // @return true if this stack is empty.
   public boolean stEmpty() {
      return stack.isEmpty();
   }

   public void push(double a) {
      stack.add(a);
   }

   public double pop() {
      if (stEmpty()) {
         throw new IndexOutOfBoundsException("Magasin on tühi");
      }
      return stack.removeLast();
   } // pop

   public void op(String s) {
      double x2 = pop();
      double x1 = pop();
      char x = s.charAt(0);
      if (s.length() < 1) throw new RuntimeException("'" + s + "' does not contain enough elements");

      switch (x) {
         case '+':
            push(x1 + x2);
            break;
         case '-':
            push(x1 - x2);
            break;
         case '*':
            push(x1 * x2);
            break;
         case '/':
            push(x1 / x2);
            break;
         default:
            throw new RuntimeException("Illegal operation '" + s + "' (valid operations are +, -, * and /).");
      }
   } //op

   public double tos() {
      if (this.stEmpty()) {
         throw new IndexOutOfBoundsException("Magasin on tühi");
      }
      return stack.getLast();
   }

   @Override
   public boolean equals(Object o) {
      if (!(o instanceof DoubleStack)) return false;
      DoubleStack oDS = (DoubleStack) o;
      if (oDS.stack.size() != stack.size()) return false;

      for (int i = 0; i < this.stack.size(); i++) {
         if (!this.stack.get(i).equals(oDS.stack.get(i))) return false;
      }
      return true;
   } // equals

   @Override
   public String toString() {
      return stack.toString();
   }

   public static double interpret(String pol) {
      if (pol.length() < 0) {
         throw new RuntimeException("Avaldise elemendid on puudu");
      }
      double returnValue = 0;
      DoubleStack stack = new DoubleStack();
      pol = pol.replaceAll("[^0-9.+\\-\\*/]+", " ").trim();
      String[] tmp = pol.split("[^0-9.+\\-\\*/]+");

      for (int i = 0; i <= tmp.length - 1; i++) {
         try {
            double x = Double.parseDouble(tmp[i]);
            stack.push(x);
         } catch (NumberFormatException ex) {
            double b = stack.pop();
            double a = stack.pop();

            switch (tmp[i].charAt(0)) {
               case '+':
                  stack.push(a + b);
                  break;
               case '-':
                  stack.push(a - b);
                  break;
               case '*':
                  stack.push(a * b);
                  break;
               case '/':
                  stack.push(a / b);
                  break;
               default:
                  throw new RuntimeException("Illegal operation " + pol + "(valid operations are +, -, * and /)");
            }
         }
      }
      if (stack.stack.size() > 1) {
         throw new RuntimeException("Stack is out of balance");
      }
      returnValue = stack.pop();
      return returnValue;
   }
}



